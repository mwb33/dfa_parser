<?php
//Mark Boady 2018
//Drexel University
//DFA Parser
//Reads DFA in custom language and makes it into DFA object.

class DFA_parser
{
	function __construct($data)
	{
		$this->data = html_entity_decode($data);
		$this->data = explode("\n",$this->data);
		for($i=0; $i < count($this->data);$i++)
		{
			$this->data[$i] = $this->clean_string($this->data[$i]);
		}
		$this->errors=array();
		$this->myDFA = NULL;
	}
	function getDFA()
	{
		return $this->myDFA;
	}
	function clean_string($str)
	{
		$str=preg_replace('/\s+/', '', $str);
		$str=strtolower($str);
		return $str;
	}
	function parse()
	{
		$this->remove_comments();
		$this->myDFA = new DFA();
		if(!$this->symbols()){return;}
		if(!$this->states()){return;}
		if(!$this->accept()){return;}
		if(!$this->start()){return;}
		$this->transitions();
	}
	function get_list($target,$name)
	{
		//Found symbols!
		$open = strpos($target,"(");
		$close = strpos($target,")");
		if($open===False)
		{
			array_push($this->errors,$name." missing open parenthesis!");
			return False;
		}
		if($close===False)
		{
			array_push($this->errors,$name." missing close parenthesis!");
			return False;
		}
		$symbols = substr($target,$open+1,$close-$open-1);
		if(strlen($symbols) < 1)
		{
			array_push($this->errors,"No Symbols in ".$name);
			return False;
		}
		$symbols = explode(",",$symbols);
		if(count($symbols) < 1)
		{
			array_push($this->errors,"Not enough Symbols in ".$name);
			return False;
		}
		return $symbols;
	}
	function get_row($code,$name)
	{
		$target="";
		foreach($this->data as $line)
		{
			if(strlen($line)>=strlen($code) && substr($line,0,strlen($code))==$code)
			{
				if(strlen($target)>0)
				{
					array_push($this->errors,"Multiple Lines Defining ".$name);
					return False;
				}else
				{
					$target=$line;
				}
			}
		}
		if(strlen($target) < 1)
		{
			array_push($this->errors,"No ".$name." Given!");
			return False;
		}
		return $target;
	}
	function symbols()
	{
		//Get Data
		$target = $this->get_row("symbols=","Alphabet");
		if($target===False){return False;}
		$symbols = $this->get_list($target,"Alphabet");
		if($symbols===False){return False;}
		//Use Data
		foreach($symbols as $s)
		{
			$this->myDFA->addSymbol($s);
		}
		return True;
	}
	function states()
	{
		//Get Data
		$target = $this->get_row("states=","State List");
		if($target===False){return False;}
		$symbols = $this->get_list($target,"State List");
		if($symbols===False){return False;}
		//Use Data
		foreach($symbols as $s)
		{
			$this->myDFA->addState($s);
		}
		return True;
	}
	function accept()
	{
		//Get Data
		$target = $this->get_row("accept=","Accept State List");
		if($target===False){return False;}
		$symbols = $this->get_list($target,"Accept State List");
		if($symbols===False){return False;}
		//Use Data
		foreach($symbols as $s)
		{
			$this->myDFA->addAccept($s);
		}
		return True;
	}
	function start()
	{
		//Get Data
		$target = $this->get_row("start=","Start State");
		if($target===False){return False;}
		$p = strpos($target,"=");
		$s = substr($target,$p+1);
		if(strlen($s)>0)
		{
			$this->myDFA->addStart($s);
			return True;
		}else
		{
			array_push($this->errors,"No Start State.");
			return False;
		}
	}
	function transitions()
	{
		//Look at each row and if it is a transition.
		foreach($this->data as $line)
		{
			$p1 = strpos($line,"if");
			$p2 = strpos($line,"and");
			$p3 = strpos($line,"then");
			if($p1!==False && $p2!=False && $p3!=False)
			{
				$from = substr($line,$p1+2,$p2-$p1-2);
				$c = substr($line,$p2+3,$p3-$p2-3);
				$to = substr($line,$p3+4);
				$this->myDFA->addTransition($from,$c,$to);
			}else{
				if($p1!==False && $p2!==False)
				{
					array_push($this->errors,"Missing then state on line ".$line);
				}
				if($p2!==False && $p3!==False)
				{
					array_push($this->errors,"Missing if state on line ".$line);
				}
				if($p1!==False && $p3!==False)
				{
					array_push($this->errors,"Missing character on line ".$line);
				}
			}
		}
	}
	function remove_comments()
	{
		for($i=0;$i < count($this->data); $i++)
		{
			$line = $this->data[$i];
			$p = strpos($line,"#");
			if($p!==False)
			{
				$line = substr($line,0,$p);
				$this->data[$i]=$line;
			}
		}
	}
	function num_errors()
	{
		return count($this->errors);
	}
	function get_errors()
	{
		$res="";
		if(count($this->errors)==0)
		{
			return "";
		}
		$res.="<div class=\"block_of_errors\">\n";
		foreach($this->errors as $e)
		{
			$res.="<span class=\"error\">".htmlentities($e)."</span>\n";
		}
		$res.="</div>\n";
		return $res;
	}
	function raw_errors()
	{
		return $this->errors;
	}
}
?>