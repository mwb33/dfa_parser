<?php
//Mark Boady
//Drexel University 2018
require_once "dfa.php";
require_once "dfa_parser.php";

//Helper Function for select options
function select_option($i)
{
	$v="example".$i.".txt";
	$n="Example ".$i;
	$res="";
	$res.="<option value=\"".htmlentities($v)."\" ";
	if(array_key_exists("select_file",$_POST) && $_POST["select_file"]==$v)
	{
		$res.=" selected=\"selected\" ";
	}
	$res.=">";
	$res.=htmlentities($n);
	$res.="</option>\n";
	return $res;
}

//If there is POST data, generate a new DFA
$parser = NULL;
$DFA = NULL;
if(array_key_exists("dfa_source",$_POST))
{
	$parser = new DFA_parser($_POST["dfa_source"]);
	$parser->parse();
	$DFA = $parser->getDFA();
	$DFA->valid();
}

//The Page has two columns.
//Column 1 is the DFA editor window
//Column 2 is the result column
?>
<html>
	<head>
		<title>DFA Development Tool</title>
		<style>
		.columnLeft {
			float: left;
			width: 500px;
			padding:3px;
		}
		.columnRight {
			float: left;
			padding:3px;
		}
		.row:after {
			content: "";
			display: table;
			clear: both;
		}
		</style>
		<script>
		function loadFile()
		{
			var file = document.getElementById("select_file");
			var filename = file.value;
			//Read the File
			var rawFile = new XMLHttpRequest();
			rawFile.open("GET",filename,true);
			rawFile.onreadystatechange = function()
			{
				if (rawFile.readyState === 4
					&& rawFile.status >=200
					&& rawFile.status < 300)
				{
					var allText = rawFile.responseText;
					//alert(allText);
					document.getElementById("dfa_source").innerHTML = allText;
				}
			}
			rawFile.send();
			
			return false;
		}
		</script>
		<?php
		if($DFA != NULL && $DFA->num_errors()==0)
		{
			echo $DFA->javascript();
		}
		?>
	</head>
	<body>
	<center><h1>DFA Generator</h1></center>
	<p>
		by Mark Boady (<a href="mailto:mwb33@drexel.edu">mwb33@drexel.edu</a>)
		at Drexel University 2018.</p>
	<div class="row">
		<div class="columnLeft">
			<!--- Edit Code Area -->
			<h2>DFA Code Area</h2>
			<p>Code your DFA in this box then hit compile.</br>
			For more details see <a href="instructions.html" target="_blank">the instructions</a>.
			</p>
			<form method="POST" action="">
			<label><b>Load Example: </b></label>
			<select name="select_file" id="select_file">
			<?php
				for($i=1; $i < 5; $i++)
				{
					echo select_option($i);
				}
			?>
			</select>
			<input type="button" name="loadfile" value="Load File" onclick="loadFile();">
			<h2>Code Edit Area</h2>
			<textarea name="dfa_source" id="dfa_source" cols="70" rows="30"><?php
				if(array_key_exists("dfa_source",$_POST))
				{
					echo htmlentities($_POST["dfa_source"]);
				}
			?></textarea>
			</br>
			<input type="submit" name="compile" value="Build DFA">
			</form>
			<!--- End Code Edit Area -->
		</div>
		<div class="columnRight">
		<?php
		if($parser==NULL || $DFA==NULL)
		{
			echo "No DFA input.";
		}
		else
		{
			if($parser->num_errors()>0 || $DFA->num_errors()>0)
			{
				echo $parser->get_errors();
				if($parser->num_errors()==0)
				{
					echo $DFA->get_errors();
				}
			}
			else{
		?>
			<h2>DFA Information</h2>
			<!-- Result Area -->
			<?php echo $DFA->formal_definition(); ?>
			</br>
			<?php echo $DFA->make_graphic(); ?>
			<div class="dfa_test_area">
			<h2>Test DFA</h2>
			<p>
				Enter a string in the below textbox as input for your DFA.</br>
				Click on "Run DFA" to see what state the DFA ends in.
			</p>
			<b>Enter Input String:</b></br>
			<input type="text" value="10111010" placeholder="Give String here" size="30" id="input_string" name="input_string">
			<input type="Button" value="Run DFA" onclick="dfa_input();"><br>
			<b>Results of Computation:</b>
			<div id="DFA_results"></div>
		<?php
			}
		}
		?>
		</div>
		<!-- End Result Area -->
	</div>
	</body>
</html>