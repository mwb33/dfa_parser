<?php
//Mark Boady 2018
//Drexel University
//DFA Simulator
//Create a DFA for simulation.


class DFA
{
	//****************************************************************
	//******************* DFA Structure ******************************
	//****************************************************************
	var $LOCATION_OF_DOT; //Where is graphviz?
	var $symbols; //Alphabet
	var $states; //List of all states
	var $start; //Start State (List with one element)
	var $accept; //Accept States
	var $transitions; //Transition Matrix [from][char]=[to]
	var $errors; //Error Messages stored here.
	
	function __construct()
	{
		$this->LOCATION_OF_DOT="/usr/bin/dot";
	
		$this->symbols=array();
		$this->states=array();
		$this->start=array();
		$this->accept=array();
		$this->transitions=array();
		$this->errors=array();
	}
	//****************************************************************
	//******************* Make DFA ***********************************
	//****************************************************************
	function addSymbol($s)
	{
		array_push($this->symbols,$s);
		sort($this->symbols);
	}
	function addState($s)
	{
		array_push($this->states,$s);
		sort($this->symbols);
	}
	function addAccept($a)
	{
		array_push($this->accept,$a);
		sort($this->accept);
	}
	function addStart($a)
	{
		array_push($this->start,$a);
		sort($this->start);
	}
	function addTransition($from,$c,$to)
	{
		
		if(array_key_exists($from,$this->transitions))
		{
			$this->transitions[$from][$c]=$to;
		}else
		{
			$this->transitions[$from]=[];
			$this->transitions[$from][$c]=$to;
		}
	}
	//****************************************************************
	//******************* Error Checking *****************************
	//****************************************************************
	function valid()
	{
		//At least one symbol
		if(count($this->symbols)<1)
		{
			array_push($this->errors,"No Symbols in Alphabet.");
			return FALSE;
		}
		//At least one state
		if(count($this->states)<1)
		{
			array_push($this->errors,"No States in DFA.");
			return FALSE;
		}
		//Exactly one start state
		//State state is a state
		if(count($this->start)!=1)
		{
			array_push($this->errors,"No Start State in DFA.");
			return FALSE;
		}
		if(!in_array($this->start[0],$this->states))
		{
			array_push($this->errors,"Start State not in state list.");
			return FALSE;
		}
		//At least one accept state
		//Accept State is a state
		if(count($this->accept)<1)
		{
			array_push($this->errors,"No Accept State in DFA.");
			return FALSE;
		}
		foreach($this->accept as $state)
		{
			if(!in_array($state,$this->states))
			{
				array_push($this->errors,"Accept State ".$state." not in state list.");
				return FALSE;
			}
		}
		//Each pair (state,symbol) has exactly one transition
		foreach($this->states as $from)
		{
			foreach($this->symbols as $char)
			{
				if(!array_key_exists($from,$this->transitions))
				{
					array_push($this->errors,"No Transitions from ".$from);
					return FALSE;
				}
				if(!array_key_exists($char,$this->transitions[$from]))
				{
					array_push($this->errors,"No Transition from ".$from." on ".$char);
					return FALSE;
				}
				$x = $this->transitions[$from][$char];
				if(!in_array($x,$this->states))
				{
					array_push($this->errors,"Transition from "
						.$from." on ".$char." goes to undefined state ".$x);
					return FALSE;
				}
			}
		}
		//Exit
		return TRUE;
	}
	function num_errors()
	{
		return count($this->errors);
	}
	function get_errors()
	{
		$res="";
		if(count($this->errors)==0)
		{
			return "";
		}
		$res.="<div class=\"block_of_errors\">\n";
		foreach($this->errors as $e)
		{
			$res.="<span class=\"error\">".htmlentities($e)."</span>\n";
		}
		$res.="</div>\n";
		return $res;
	}
	//****************************************************************
	//******************* Display as Javascript **********************
	//****************************************************************
	function quote_array($arr)
	{
		$res="";
		for($i=0; $i < count($arr); $i++)
		{
			$res.="\"".$arr[$i]."\"";
			if($i+1<count($arr))
			{
				$res.=",";
			}
		}
		return $res;
	}
	function js_dict()
	{
		$res="{\n";
		$state_list = array_keys($this->transitions);
		for($i=0; $i < count($state_list); $i++)
		{
			$state = $state_list[$i];
			$res.="\t\t\t\"".$state."\":";
			$char_list = array_keys($this->transitions[$state]);
			$res.="{";
			for($j=0; $j < count($char_list); $j++)
			{
				$char=$char_list[$j];
				$res.="\"".$char."\":\"".$this->transitions[$state][$char]."\"";
				if($j+1 < count($char_list))
				{
					$res.=",";
				}
			}
			$res.="}";
			if($i+1 < count($state_list))
			{
				$res.=",";
			}
			$res.="\n";
		}
		$res.="\t\t}\n";
		return $res;
		
	}
	function javascript()
	{
		$res="";
		$res.="<script>\n";
		$res.="\tfunction dfa_input()\n";
		$res.="\t{\n";
		$res.="\t\tvar results=\"Exeuction Failed!\";\n";
		$res.="\t\t//Get Text\n";
		$res.="\t\tvar input = document.getElementById(\"input_string\");\n";
		$res.="\t\tinput = input.value;\n";
		$res.="\t\tinput = input.toLowerCase();\n";
		$res.="\t\t//Make DFA\n";
		//$res.="\t\tvar sigma = [".$this->quote_array($this->symbols)."];\n";
		//$res.="\t\tvar states = [".$this->quote_array($this->states)."];\n";
		$res.="\t\tvar start = \"".$this->start[0]."\";\n";
		$res.="\t\tvar accept = [".$this->quote_array($this->accept)."];\n";
		$res.="\t\tvar transitions=".$this->js_dict();
		$res.="\t\t//Simulate DFA\n";
		$res.="\t\tvar failure=false;\n";
		$res.="\t\tvar current_state = start;\n";
		$res.="\t\tfor(var i=0;i < input.length;i++)\n";
		$res.="\t\t{\n";
		$res.="\t\t\tvar T = transitions[current_state];\n";
		$res.="\t\t\tvar c = input[i];\n";
		$res.="\t\t\tif(c in T)\n";
		$res.="\t\t\t{\n";
		$res.="\t\t\t\t\tcurrent_state = T[c];\n";
		$res.="\t\t\t}else\n";
		$res.="\t\t\t{\n";
		$res.="\t\t\t\tfailure=true;\n";
		$res.="\t\t\t\tresults=\"No Transition from \"+current_state+\" on \"+c;\n";
		$res.="\t\t\t}\n";
		$res.="\t\t\t}\n";
		$res.="\t\t//Check Conclusion\n";
		$res.="\t\tif(!failure)\n";
		$res.="\t\t{\n";
		$res.="\t\t\tif(accept.includes(current_state))\n";
		$res.="\t\t\t{\n";
		$res.="\t\t\t\tresults=\"Accept String. Final State was \"+current_state;\n";
		$res.="\t\t\t}else\n";
		$res.="\t\t\t{\n";
		$res.="\t\t\t\tresults=\"Reject String. Final State was \"+current_state;\n";
		$res.="\t\t\t}\n";
		$res.="\t\t}\n";
		$res.="\t\t//Write Results\n";
		$res.="\t\tvar output = document.getElementById(\"DFA_results\");\n";
		$res.="\t\toutput.innerHTML = results;\n";
		$res.="\t\treturn false;\n";
		$res.="\t}\n";
		$res.="</script>\n";
		return $res;

	}
	//****************************************************************
	//******************* Display as Graphviz ************************
	//****************************************************************
	function graph_source()
	{
		$res="";
		$res.="digraph\n";
		$res.="{";
		$res.="\trankdir=LR\n";
		$res.="\tSTART [label=\"Start\", shape=\"none\"]\n";
		foreach($this->states as $s)
		{
			if(in_array($s,$this->accept))
			{
				$shape="doublecircle";
			}else
			{
				$shape="circle";
			}
			$res.="\t".$s." [label=\"".$s."\", shape=\"".$shape."\"]\n";
			
		}
		//Start State
		$res.="\tSTART -> ".$this->start[0]."\n";
		//Transitions
		foreach($this->states as $s)
		{
			foreach($this->symbols as $a)
			{
				if(array_key_exists($s,$this->transitions))
				{
					$res.="\t".$s." -> ".$this->transitions[$s][$a]
						." [label=\"".$a."\"]\n";
				}
			}
		}
		//Close Up
		$res.="}";
		return $res;
	}
	function make_graphic()
	{
		$data = $this->graph_source();
		$stream = fopen("php://temp","r+");
		fwrite($stream,$data);
		rewind($stream);
	
		$dspec=array(
			0=>$stream,
			1=>array("pipe","w"),
			2=>array("pipe","w")
		);
		$pp = proc_open($this->LOCATION_OF_DOT.' -Tsvg',$dspec,$pipes);
		//Busy Wait for my image
		do {
			usleep(10000);
			$stat = proc_get_status($pp);
		} while($stat and $stat['running']);
		//Find out the results
		$graph = stream_get_contents($pipes[1]);
		$errors = stream_get_contents($pipes[2]);
		if(strlen($errors)>0)
		{
			return "Could Not Generate Image: <br>".$errors;
		}else
		{
			return $graph;
		}
	}
	//****************************************************************
	//******************* Display as HTML ****************************
	//****************************************************************
	function formal_definition()
	{
		$res="";
		$res.="<table>\n";
		$res.="<tr><th colspan=\"2\" style=\"text-align:center\">"
			."DFA Formal Definition</th></td>\n";
		$res.="<tr><td><b>Alphabet (Sigma):</b></td>";
		$res.="<td>{".htmlentities(implode(",",$this->symbols))."}</td>";
		$res.="</tr>\n";
		$res.="<tr><td><b>States:</b></td>";
		$res.="<td>{".htmlentities(implode(",",$this->states))."}</td>";
		$res.="</tr>\n";
		$res.="<tr><td><b>Start State:</b></td>";
		$res.="<td>{".htmlentities(implode(",",$this->start))."}</td>";
		$res.="</tr>\n";
		$res.="<tr><td><b>Accept States:</b></td>";
		$res.="<td>{".htmlentities(implode(",",$this->accept))."}</td>";
		$res.="</tr>\n";
		$res.="<tr><td><b>Transition Table:</b></td>";
		$res.="<td>".$this->html_transitions()."</td>";
		$res.="</tr>\n";
		$res.="</table>\n";
		return $res;
	}
	function html_transitions()
	{
		$res="\n\n";
		$res.="<!--- DFA Transition Table ---!>\n";
		$res.="<table border=\"1\" style=\"border-collapse: collapse;\">\n";
		//Symbol Row
		$res.="\t<tr>\n";
		$res.="\t\t<th>State/Char</th>\n";
		foreach($this->symbols as $a)
		{
			$res.="\t\t<th>".htmlentities($a)."</th>\n";
		}
		$res.="\t</tr>\n";
		//End Symbols
		foreach($this->states as $s)
		{
			$res.="\t<tr>\n";
			$res.="\t\t<th>".htmlentities($s)."</th>\n";
			foreach($this->symbols as $a)
			{
				if(array_key_exists($s,$this->transitions))
				{
					$res.="\t\t<td>".htmlentities($this->transitions[$s][$a])."</td>\n";
				}else
				{
					$res.="\t\t<td>&nbsp;</td>\n";
				}
			}
			$res.="\t<tr>\n";
		}
		//Wrap Up
		$res.="</table>\n";
		$res.="<!--- End DFA Transition Table ---!>\n\n";
		return $res;
	}
	//****************************************************************
	//****************** Needed for Autograding **********************
	//****************************************************************
	function raw_errors()
	{
		return $this->errors;
	}
	function simulate($input)
	{
		$current_state=$this->start[0];
		for($i=0; $i < strlen($input); $i++)
		{
			$c = $input[$i];
			if(!in_array($c,$this->symbols))
			{
				return False;
			}
			$current_state = $this->transitions[$current_state][$c];
		}
		if(in_array($current_state,$this->accept))
		{
			return True;
		}
		return False;
	}
	//****************************************************************
	//******************* End of Class Def ***************************
	//****************************************************************
}

?>
